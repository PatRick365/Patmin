from sqlalchemy import ForeignKey, Column, Integer, String, Date
from database import Base
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return '<User(name="%s")>' % self.name


class Expense(Base):
    __tablename__ = 'expense'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    date = Column(Date)
    comment = Column(String)
    amount = Column(Integer)

    user = relationship("User", back_populates="expense")

    def __repr__(self):
        return '<User(user_id="%s", date="%s", comment="%s", amount="%s")>' % (self.user_id, self.date, self.comment, self.amount)