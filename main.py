from datetime import date
import collections

from flask import Flask, redirect, url_for, render_template, request
from sqlalchemy.orm import relationship

import models
from database import SessionLocal, engine


app = Flask(__name__)
session = SessionLocal()


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


# Return a string describing total money, who owes money, who gets money
def get_money_text(dict_money):
    if not dict_money:
        return ''

    sum_money = sum(dict_money.values())
    count_people = len(dict_money.keys())
    share = sum_money / count_people
    text_money = f'Total: {sum_money}€. '

    if sum_money == 0:
        return text_money

    for person, money in dict_money.items():
        gets = money - share
        if gets > 0:
            if count_people > 2:
                text_money += f'{person} gets {gets:.2f}€. '
        else:
            text_money += f'{person} owes {abs(gets):.2f}€. '

    return text_money


@app.route('/', methods=['POST', 'GET'])
def home():
    today = date.today()
    return redirect(url_for('table', str_month=str(today.month).rjust(2, '0'), str_year=str(today.year)))


@app.route('/expense_form/<str_month>/<str_year>/<str_expense_id>', methods=['POST', 'GET'])
def expense_form(str_month, str_year, str_expense_id):
    print(f'/expense_form method: {request.method} date: {str_month}/{str_year} expense_id: {str_expense_id}')

    try:
        month = int(str_month)
        year = int(str_year)
        expense_id = int(str_expense_id)
    except ValueError:
        return redirect(url_for('#'))

    back_to_table = redirect(url_for('table', str_month=str(month).rjust(2, '0'), str_year=str(year)))

    if request.method == 'POST':
        print(request.form)

        if not request.form['button']:
            return redirect('#')

        if request.form['button'] == 'action-cancel':
            return back_to_table

        if request.form['button'] == 'action-delete':
            expense = session.query(models.Expense).get(expense_id)

            if not expense:
                print(f'expense with id {expense_id} not found.')
                return back_to_table

            session.delete(expense)
            session.commit()
            return back_to_table

        if request.form['button'] == 'action-post-form':
            user_id = session.query(models.User.id).filter(models.User.name == request.form['input-person'])

            if not user_id:
                print(f'user with name {request.form["input-person"]} not found.')
                return back_to_table

            if not is_int(request.form['input-amount']):
                return redirect('#')

            if expense_id > 0:
                expense = session.query(models.Expense).get(expense_id)

                if not expense:
                    print(f'expense with id {expense_id} not found.')
                    return back_to_table

                expense.user_id = user_id
                expense.date = date(year, month, 1)
                expense.comment = request.form['input-comment']
                expense.amount = int(request.form['input-amount'])
                session.add(expense)
            else:
                expense = models.Expense(user_id=user_id, date=date(year, month, 1),
                                         comment=request.form['input-comment'],
                                         amount=int(request.form['input-amount']))
                session.add(expense)

            session.commit()

            return back_to_table

    if request.method == 'GET':
        user_names = [user.name for user in session.query(models.User.name)]

        if int(str_expense_id) > 0:
            expense = session.query(models.Expense).get(int(str_expense_id))

            if expense:
                print(expense)
                return render_template('expense_form.html', edit_mode=True, list_persons=user_names, text_comment=expense.comment,
                                       text_amount=expense.amount, person_selected=expense.user.name)
            else:
                print(f'expense with id {str_expense_id} not found')
                return redirect('#')

        return render_template('expense_form.html', edit_mode=False, list_persons=user_names)
    return redirect('#')


@app.route('/<str_month>/<str_year>', methods=['POST', 'GET'])
def table(str_month, str_year):
    print(f'/ method: {request.method} date: {str_month}/{str_year}')

    try:
        month = int(str_month)
        year = int(str_year)
    except ValueError:
        return redirect('/')

    if request.method == 'POST':
        print(request.form)

        if 'button-edit-row' in request.form:
            if is_int(request.form['button-edit-row']):
                return redirect(url_for('expense_form', str_month=str(month).rjust(2, '0'), str_year=str(year), str_expense_id=int(request.form['button-edit-row'])))
            else:
                print('button-edit-row is not an int: ' + request.form['button-edit-row'])

        if 'button' in request.form:
            if request.form['button'] == 'action-month-prev':
                month -= 1
                if month < 1:
                    month = 12
                    year -= 1
            elif request.form['button'] == 'action-month-next':
                month += 1
                if month > 12:
                    month = 1
                    year += 1
            elif request.form['button'] == 'action-new-expense':
                return redirect(url_for('expense_form', str_month=str(month).rjust(2, '0'), str_year=str(year), str_expense_id=0))
            else:
                print('invalid button value: ' + str(request.form['button']))
                return redirect('#')

        return redirect(url_for('table', str_month=str(month).rjust(2, '0'), str_year=str(year)))

    dict_user_spent = {user.name: 0 for user in session.query(models.User.name)}
    query = session.query(models.Expense).order_by(models.Expense.id).filter(models.Expense.date == date(year, month, 1))

    content = '<tr><th scope="col">Person</th><th scope="col">Comment</th><th scope="col">Amount</th><th scope="col">Edit</th></tr>'
    for expense in query:
        content += f'<tr><td>{expense.user.name}</td>'
        content += f'<td>{expense.comment}</td>'
        content += f'<td>{expense.amount}€</td>'
        content += f'<td><button type="submit" class="btn btn-secondary" name="button-edit-row" value="{expense.id}">Edit</button><td></tr>'

        dict_user_spent[expense.user.name] += expense.amount

    text_date = date(year, month, 1).strftime('%b/%Y')
    text_money = get_money_text(dict_user_spent)

    return render_template('table.html', content=content, text_date=text_date, text_money=text_money)


@app.route('/chart', methods=['GET'])
def chart():
    print(f'/chart method: {request.method}')

    data = collections.OrderedDict()

    for expense in session.query(models.Expense).order_by(models.Expense.date):
        key = expense.date.strftime('%b/%y')
        if key not in data:
            data[key] = 0
        data[key] += expense.amount

    return render_template('chart.html', values=data.values(), labels=data.keys(), legend='Expenses')


if __name__ == '__main__':
    models.Base.metadata.create_all(bind=engine)
    models.User.expense = relationship('Expense', order_by=models.Expense.id, back_populates='user')

    app.run(host='0.0.0.0', debug=True)

