#!/usr/bin/env bash

nohup project_env/bin/python3 main.py >> "log/log_$(date +"%Y-%m-%d_%H-%M-%S").log" 2>&1 &
